<?php

declare(strict_types=1);

namespace Drupal\group_permission_set;

// Drupal modules.
use Drupal\group_permission_set\Access\GroupPermissionChecker;
use Drupal\group_permission_set\Access\IndividualGroupPermissionCalculator;
use Drupal\group_permission_set\Access\SynchronizedGroupPermissionCalculator;
// Drupal core.
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
// External libraries.
use Symfony\Component\DependencyInjection\Reference;

/**
 * Service provider for the Group Permission Set module.
 *
 * Changes the classses for services that we provide custom/overridden
 * implementations.
 *
 * @see \Drupal\group_permission_set\Access\GroupPermissionChecker
 * @see \Drupal\group_permission_set\Access\IndividualGroupPermissionCalculator
 * @see \Drupal\group_permission_set\Access\SynchronizedGroupPermissionCalculator
 */
class GroupPermissionSetServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container
      ->getDefinition('group_permission.checker')
      ->setClass(GroupPermissionChecker::class)
      ->addArgument(new Reference('entity_type.manager'));

    $container
      ->getDefinition('group_permission.individual_calculator')
      ->setClass(IndividualGroupPermissionCalculator::class);

    $container
      ->getDefinition('group_permission.synchronized_calculator')
      ->setClass(synchronizedGroupPermissionCalculator::class);
  }

}

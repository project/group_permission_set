<?php

declare(strict_types=1);

namespace Drupal\group_permission_set\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginBase;
use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides entity reference selection for group permission sets.
 *
 * It restricts permission set selection to the ones that apply to the type of
 * the given group entity. Meant to be used for selecting the permission set in
 * the entity reference field on the group entity.
 *
 * @EntityReferenceSelection(
 *   id = "group_permission_set",
 *   label = @Translation("Group permission set"),
 *   group = "group_permission_set",
 *   entity_types = {"group_permission_set"},
 *   weight = 0,
 * )
 */
class PermissionSetSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'target_type' => 'group_permission_set',
    ] + SelectionPluginBase::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state,
  ) {
    return SelectionPluginBase::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state,
  ) {
    SelectionPluginBase::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery(
    $match = NULL,
    $match_operator = 'CONTAINS',
  ) {
    $configuration = $this->getConfiguration();
    $target_type = $configuration['target_type'];
    $entity_type = $this->entityTypeManager->getDefinition($target_type);

    if ($target_type !== 'group_permission_set') {
      throw new \Exception(
        'The "group_permission_set" entity reference selection plugin can only be used for permission set selection.'
      );
    }

    $query = $this->entityTypeManager->getStorage($target_type)->getQuery();
    $query->accessCheck(TRUE);

    // Restrict to permission sets that apply to the configured group type.
    $query->condition('group_type_id', $configuration['entity']->bundle());

    // Add entity-access tag.
    $query->addTag("{$target_type}_access");

    // Add the Selection handler for system_query_entity_reference_alter().
    $query->addTag('entity_reference');
    $query->addMetaData('entity_reference_selection_handler', $this);

    if (isset($match) && $label_key = $entity_type->getKey('label')) {
      $query->condition($label_key, $match, $match_operator);
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function createNewEntity($entity_type_id, $bundle, $label, $uid) {
    throw new \RuntimeException(
      'The group permission set entity reference selection does not support auto-creating new permission sets.',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validateReferenceableNewEntities(array $entities) {
    throw new \RuntimeException(
      'The group permission set entity reference selection does not support auto-creating new permission sets.',
    );
  }

}

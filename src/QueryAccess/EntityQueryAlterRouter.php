<?php

declare(strict_types=1);

namespace Drupal\group_permission_set\QueryAccess;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Routes entity queries to the appropriate class ofr performing alterations.
 */
class EntityQueryAlterRouter {

  /**
   * Constructs a new EntityQueryAlterRouter object.
   *
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $classResolver
   *   The class resolver.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    protected ClassResolverInterface $classResolver,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
  }

  /**
   * Determines the alterer to be used for the given query.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The query to be altered. It must be an entity query tagged for access
   *   control i.e. "{$entity_type_id}_access".
   * @param string $entity_type_id
   *   The entity type ID of the entity being queried.
   *
   * @return string|null
   *   The fully qualified class name of the alterer to use for the given query,
   *   or `NULL` if no alterations should be made for the query.
   *
   * @see hook_query_TAG_alter()
   * @see group_query_entity_query_alter()
   */
  public function route(
    SelectInterface $query,
    string $entity_type_id,
  ): string|null {
    return match ($entity_type_id) {
      'group' => GroupQueryAlter::class,
      'group_relationship' => GroupRelationshipQueryAlter::class,
      default => EntityQueryAlter::class,
    };
  }

  /**
   * Applies the alterations to the given query.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The query to be altered. It must be an entity query tagged for access
   *   control i.e. "{$entity_type_id}_access".
   * @param string $entity_type_id
   *   The entity type ID of the entity being queried.
   *
   * @see hook_query_TAG_alter()
   * @see group_query_entity_query_alter()
   */
  public function alter(
    SelectInterface $query,
    string $entity_type_id,
  ): void {
    $class_name = $this->route($query, $entity_type_id);
    if ($class_name === NULL) {
      return;
    }

    $this->classResolver
      ->getInstanceFromDefinition($class_name)
      ->alter(
        $query,
        $this->entityTypeManager->getDefinition($entity_type_id),
      );
  }

}

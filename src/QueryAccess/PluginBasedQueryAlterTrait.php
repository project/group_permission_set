<?php

declare(strict_types=1);

namespace Drupal\group_permission_set\QueryAccess;

use Drupal\group\PermissionScopeInterface;
use Drupal\Core\Database\Query\ConditionInterface;

/**
 * Provides methods for plugin-based query alterers.
 *
 * These are entities related to groups, and relationship entities. It excludes
 * group entities themselves.
 */
trait PluginBasedQueryAlterTrait {

  use QueryAlterTrait;

  /**
   * {@inheritdoc}
   *
   * The IDs to grant access to should be the IDs of the permission sets,
   * grouped by plugin ID.
   */
  protected function addSynchronizedConditionsForPermissionSets(
    array $allowed_ids,
    ConditionInterface $scope_conditions,
    $scope,
  ): void {
    $data_table = $this->getPluginDataTable();
    $membership_alias = $this->ensureMembershipJoin();

    $set_table = $this->leftJoinPermissionSetTable($data_table, 'gid');

    $sub_condition = $this->query->andConditionGroup();
    $scope_conditions->condition($sub_condition);

    // If this is an insider scope the user must have a membership; otherwise
    // not.
    if ($scope === PermissionScopeInterface::OUTSIDER_ID) {
      $sub_condition->isNull("$membership_alias.entity_id");
    }
    else {
      $sub_condition->isNotNull("$membership_alias.entity_id");
    }

    $or_condition = $this->query->orConditionGroup();
    $sub_condition->condition($or_condition);

    // We collect all relationship types per permission set that grant access to
    // the group. We then alter the query to include relationships of the
    // determined relationship types in groups that have their permissions
    // determined by the corresponding set.
    $property_column = $this->getPermissionSetPropertyColumn();
    $relationship_type_ids = $this->getRelationshipTypeIds($allowed_ids);
    foreach ($relationship_type_ids as $set_id => $set_type_ids) {
      $and_subcondition = $this->query->andConditionGroup();
      $or_condition->condition($and_subcondition);

      $and_subcondition->condition("{$set_table}.{$property_column}", $set_id);
      $and_subcondition->condition("{$data_table}.type", $set_type_ids, 'IN');
    }
  }

  /**
   * Returns the IDs of the relationship types for the given identifiers.
   *
   * @param array $allowed_ids
   *   The IDs to grant access to; these should be arrays keyed by the plugin ID
   *   and containing the permission set IDs.
   *
   * @return array
   *   The IDs of the relationship types.
   *
   * @throws \RuntimeException
   *   When a permission set does not reference a group type.
   */
  protected function getRelationshipTypeIds(
    array $allowed_ids,
  ): array {
    $relationship_storage = $this->entityTypeManager
      ->getStorage('group_relationship_type');
    $set_storage = $this->entityTypeManager
      ->getStorage('group_permission_set');

    // A list of plugin IDs and group types can be optimized into a list of
    // group relationship type IDs. This to avoid having to add an `IN` query
    // per plugin ID as seen in
    // `Drupal\group\QueryAccess\PluginBasedQueryAlterBase::addIndividualConditions()`.
    $relationship_type_ids = [];
    foreach ($allowed_ids as $plugin_id => $set_ids) {
      foreach (array_unique($set_ids) as $set_id) {
        // phpcs:disable
        // @I Statically cache the group types for all sets
        //    type     : improvement
        //    priority : normal
        //    labels   : performance
        // phpcs:enable
        $group_type_id = $set_storage->load($set_id)->get('group_type_id');
        if (empty($group_type_id)) {
          throw new \RuntimeException(sprintf(
            'No group type defined for permission set with ID "%s".',
            $set_id,
          ));
        }

        if (!isset($relationship_type_ids[$set_id])) {
          $relationship_type_ids[$set_id] = [];
        }
        $relationship_type_ids[$set_id][] = $relationship_storage->getRelationshipTypeId(
          $group_type_id,
          $plugin_id,
        );
      }
    }

    return $relationship_type_ids;
  }

}

<?php

declare(strict_types=1);

namespace Drupal\group_permission_set\QueryAccess;

use Drupal\group\QueryAccess\GroupRelationshipQueryAlter as GroupRelationshipQueryAlterBase;

/**
 * Alters relationship queries to take into account group permission sets.
 *
 * The group module alters queries to only include relationships that the user
 * should have access to based on group permissions defined by group type and by
 * individual memberships in groups. We additionally alter queries to take into
 * account groups that have their permissions determined by permission sets
 * instead of the permissions defined by the group type.
 */
class GroupRelationshipQueryAlter extends GroupRelationshipQueryAlterBase {

  use PluginBasedQueryAlterTrait;

  /**
   * {@inheritdoc}
   */
  protected function getFilterIdentifiersMethod(): string {
    return 'filterIdentifiersForPlugins';
  }

}

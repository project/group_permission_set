<?php

declare(strict_types=1);

namespace Drupal\group_permission_set\QueryAccess;

use Drupal\Core\Database\Query\ConditionInterface;
use Drupal\group\PermissionScopeInterface;
use Drupal\group\QueryAccess\GroupQueryAlter as GroupQueryAlterBase;

/**
 * Alters group entity queries to take into account group permission sets.
 *
 * The group module alters queries to only include groups that the user should
 * have access to based on group permissions defined by group type and by
 * individual memberships in groups. We additionally alter queries to take into
 * account groups that have their permissions determined by permission sets
 * instead of the permissions defined by the group type.
 */
class GroupQueryAlter extends GroupQueryAlterBase {

  use QueryAlterTrait;

  /**
   * {@inheritdoc}
   *
   * The IDs to grant access to should be the IDs of the permission sets.
   */
  protected function addSynchronizedConditionsForPermissionSets(
    array $allowed_ids,
    ConditionInterface $scope_conditions,
    $scope,
  ): void {
    $membership_alias = $this->ensureMembershipJoin();
    $table_with_type = $this->getTableWithType();

    $set_table = $this->leftJoinPermissionSetTable($table_with_type, 'id');

    $sub_condition = $this->query->andConditionGroup();
    $scope_conditions->condition($sub_condition);

    // If this is an insider scope the user must have a membership; otherwise
    // not.
    if ($scope === PermissionScopeInterface::OUTSIDER_ID) {
      $sub_condition->isNull("$membership_alias.entity_id");
    }
    else {
      $sub_condition->isNotNull("$membership_alias.entity_id");
    }

    $or_condition = $this->query->orConditionGroup();
    $sub_condition->condition($or_condition);

    // We collect all permission sets per group type that grant access to the
    // group. We then alter the query to include groups that have their
    // permissions determined by one of the determined permission sets.
    $property_column = $this->getPermissionSetPropertyColumn();
    $set_ids_by_group_type = $this->getPermissionSetIds($allowed_ids);
    foreach ($set_ids_by_group_type as $group_type_id => $set_ids) {
      $and_subcondition = $this->query->andConditionGroup();
      $and_subcondition->condition("{$set_table}.{$property_column}", $set_ids, 'IN');
      $and_subcondition->condition("{$table_with_type}.type", $group_type_id);
      $or_condition->condition($and_subcondition);
    }
  }

  /**
   * Returns the IDs of the permission sets for the given identifiers.
   *
   * @param array $allowed_ids
   *   The IDs to grant access to; these should be the permission set IDs.
   *
   * @return array
   *   An array containing arrays each one keyed by group type and containing
   *   the IDs of the permission sets.
   *
   * @throws \RuntimeException
   *   When a permission set does not reference a group type.
   */
  protected function getPermissionSetIds(array $allowed_ids): array {
    $set_storage = $this->entityTypeManager->getStorage('group_permission_set');

    $set_ids = [];
    foreach (array_unique($allowed_ids) as $set_id) {
      $group_type_id = $set_storage->load($set_id)->get('group_type_id');
      if (empty($group_type_id)) {
        throw new \RuntimeException(sprintf(
          'No group type defined for permission set with ID "%s".',
          $set_id,
        ));
      }

      if (!isset($set_ids[$group_type_id])) {
        $set_ids[$group_type_id] = [];
      }
      $set_ids[$group_type_id][] = $set_id;
    }

    return $set_ids;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFilterIdentifiersMethod(): string {
    return 'filterIdentifiers';
  }

}

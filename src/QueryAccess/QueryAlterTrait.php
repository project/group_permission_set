<?php

declare(strict_types=1);

namespace Drupal\group_permission_set\QueryAccess;

use Drupal\group\PermissionScopeInterface;
use Drupal\group_permission_set\Constant\Field\Group as GroupField;
use Drupal\Core\Database\Query\ConditionInterface;

/**
 * Provides methods for query alterers.
 */
trait QueryAlterTrait {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The query to alter.
   *
   * @var \Drupal\Core\Database\Query\SelectInterface
   */
  protected $query;

  /**
   * Returns the method to use for filtering identifiers.
   *
   * The method must be splitting the identifiers into bundle and permission set
   * identifiers. The structure of the identifiers depends on the scope and the
   * entity being queried; different query alterers may therefore need to use
   * different methods to separate the bundle from the permission set
   * identifiers.
   *
   * We could use a separate class/interface for this, but there's only a couple
   * of known cases so far so there's no need to make things more complicated.
   * We may do so when we write tests, if it helps with better structuring and
   * testing the code.
   *
   * @return string
   *   The method name.
   */
  abstract protected function getFilterIdentifiersMethod(): string;

  /**
   * Adds conditions for a synchronized scope based on permission sets.
   *
   * @param array $allowed_ids
   *   The IDs to grant access to; the format that these are provided is
   *   deteremined by the implented class.
   * @param \Drupal\Core\Database\Query\ConditionInterface $scope_conditions
   *   The condition group to add the access checks to.
   * @param string $scope
   *   The name of the synchronized scope, either 'outsider' or 'insider'.
   */
  abstract protected function addSynchronizedConditionsForPermissionSets(
    array $allowed_ids,
    ConditionInterface $scope_conditions,
    $scope,
  ): void;

  /**
   * Returns the name of the table for the `group_permission_set` field.
   *
   * The field is always on the `group` entity.
   *
   * @return string
   *   The table name.
   */
  protected function getPermissionSetTable(): string {
    return $this->entityTypeManager
      ->getStorage('group')
      ->getTableMapping()
      ->getFieldTableName(GroupField::PERMISSION_SET);
  }

  /**
   * Adds a left join to the table of the `group_permission_set` field.
   *
   * @return string
   *   The table to add the join to.
   */
  protected function leftJoinPermissionSetTable(
    string $table,
    string $gid_column,
  ): string {
    return $this->query->leftJoin(
      $this->getPermissionSetTable(),
      'gps',
      "{$table}.{$gid_column} = %alias.entity_id",
    );
  }

  /**
   * Returns the name of the column for the entity reference target property.
   */
  protected function getPermissionSetPropertyColumn(): string {
    return GroupField::PERMISSION_SET . '_target_id';
  }

  /**
   * Adds conditions for all scopes given a set of IDs where access is array.
   *
   * @param array $allowed_ids
   *   A set of scope identifiers where access is granted for each scope. Keys
   *   are scope names and values are determined by the implementing class.
   * @param \Drupal\Core\Database\Query\ConditionInterface $parent_condition
   *   The parent condition to add the subconditions to.
   *
   * @see \Drupal\group\QueryAccess\QueryAlterBase::addScopedConditions()
   */
  protected function addScopedConditions(
    array $allowed_ids,
    ConditionInterface $parent_condition,
  ) {
    $scope_conditions = $this->ensureOrConjunction($parent_condition);

    // Add the group types where synchronized access is granted.
    foreach (PermissionScopeInterface::SYNCHRONIZED_IDS as $scope) {
      if (empty($allowed_ids[$scope])) {
        continue;
      }

      $method = $this->getFilterIdentifiersMethod();
      [$bundles, $sets] = $this->{$method}($allowed_ids[$scope]);

      if (!empty($bundles)) {
        $this->addSynchronizedConditions(
          $bundles,
          $scope_conditions,
          $scope,
        );
      }
      if (!empty($sets)) {
        $this->addSynchronizedConditionsForPermissionSets(
          $sets,
          $scope_conditions,
          $scope,
        );
      }
    }

    // Add the groups where individual access is granted.
    if (!empty($allowed_ids[PermissionScopeInterface::INDIVIDUAL_ID])) {
      $this->addIndividualConditions(
        $allowed_ids[PermissionScopeInterface::INDIVIDUAL_ID],
        $scope_conditions,
      );
    }
  }

  /**
   * Returns the given identifiers split into bundle and permission set arrays.
   *
   * Note that this is relevant only to synchronized scopes; it does not apply
   * and should not be used for the individual scope.
   *
   * @param array $allowed_ids
   *   The identifiers for a specific scope (e.g. `outsider`, `insider` etc.),
   *   keyed by plugin ID.
   *
   * @return array
   *   An array containing two items in the following order:
   *   - An array keyed by plugin ID and containing the bundle identifiers.
   *   - An array keyed by plugin ID and containing the permission set
   *     identifiers with the `set:` prefix removed i.e.containing the
   *     permission set IDs directly.
   *
   * @see \Drupal\group\QueryAccess\QueryAlterBase::addScopedConditions()
   */
  protected function filterIdentifiersForPlugins(array $allowed_ids): array {
    $bundles = [];
    $sets = [];

    foreach ($allowed_ids as $plugin_id => $identifiers) {
      [
        $bundles[$plugin_id],
        $sets[$plugin_id],
      ] = $this->filterIdentifiers($identifiers);
    }

    return [array_filter($bundles), array_filter($sets)];
  }

  /**
   * Returns the given identifiers split into bundle and permission set arrays.
   *
   * Note that this is relevant only to synchronized scopes; it does not apply
   * and should not be used for the individual scope.
   *
   * @param array $identifiers
   *   The identifiers for a specific scope (e.g. `outsider`, `insider` etc.)
   *   and a specific group relation type plugin.
   *
   * @return array
   *   An array containing two items in the following order:
   *   - An array containing the bundle identifiers.
   *   - An array containing the permission set identifiers with the `set:`
   *     prefix removed i.e.containing the permission set IDs directly.
   */
  protected function filterIdentifiers(array $identifiers): array {
    $bundles = [];
    $sets = [];

    foreach ($identifiers as $identifier) {
      if (str_starts_with($identifier, 'set:')) {
        $sets[] = explode(':', $identifier)[1];
        continue;
      }

      $bundles[] = $identifier;
    }

    return [$bundles, $sets];
  }

}

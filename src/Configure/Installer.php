<?php

declare(strict_types=1);

namespace Drupal\group_permission_set\Configure;

use Drupal\field\Entity\FieldStorageConfig;
use Drupal\group_permission_set\Constant\Entity\Type as EntityType;
use Drupal\group_permission_set\Constant\Field\Group as GroupField;
use Drupal\group\Entity\GroupTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;

/**
 * Default implementation of the Group Permission Set Installer.
 */
class Installer implements InstallerInterface {

  /**
   * Constructs a new Installer object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EntityFieldManagerInterface $entityFieldManager
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function installForGroupType(
    GroupTypeInterface $group_type,
    bool $save = TRUE
  ): void {
    if ($this->isInstalledForGroupType($group_type)) {
      return;
    }

    $group_type->setThirdPartySetting(
      'group_permission_set',
      'status',
      TRUE
    );
    if ($save) {
      $this->entityTypeManager->getStorage('group_type')->save($group_type);
    }

    $this->installField($group_type);
  }

  /**
   * {@inheritdoc}
   */
  public function uninstallForGroupType(
    GroupTypeInterface $group_type,
    bool $save = TRUE
  ): void {
    $group_type->setThirdPartySetting(
      'group_permission_set',
      'status',
      FALSE
    );
    if ($save) {
      $this->entityTypeManager->getStorage('group_type')->save($group_type);
    }

    $this->uninstallField($group_type);
  }

  /**
   * {@inheritdoc}
   */
  public function isInstalledForGroupType(
    GroupTypeInterface $group_type
  ): bool {
    $status = $group_type->getThirdPartySetting(
      'group_permission_set',
      'status'
    );
    if (!$status) {
      return FALSE;
    }

    $field_definitions = $this->entityFieldManager->getFieldDefinitions(
      'group',
      $group_type->id()
    );
    return in_array(GroupField::PERMISSION_SET, array_keys($field_definitions));
  }

  /**
   * {@inheritdoc}
   */
  public function canUninstallForGroupType(
    GroupTypeInterface $group_type
  ): bool {
    // We cannot uninstall if there are permission sets for the group type.
    $count = $this->entityTypeManager
      ->getStorage(EntityType::PERMISSION_SET)
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('group_type_id', $group_type->id())
      ->count()
      ->execute();

    return $count ? FALSE : TRUE;
  }

  /**
   * Installs the permission set field on the given group type.
   *
   * @param Drupal\group\Entity\GroupTypeInterface $group_type
   *   The group type.
   *
   * @see \Drupal\group_permission_set\Constant\Field\Group::PERMISSION_SET
   */
  protected function installField(GroupTypeInterface $group_type): void {
    // Create the field storage configuration if it is the first time that we
    // install the field on any group type.
    $storage_config = $this->installFieldStorage();

    // Create the field configuration if it doesn't exist already on the given
    // group type.
    $config_storage = $this->entityTypeManager->getStorage('field_config');
    $config = $config_storage->load(
      'group.' . $group_type->id() . '.' . GroupField::PERMISSION_SET
    );
    if ($config) {
      return;
    }

    $config = $config_storage->create([
      'field_storage' => $storage_config,
      'bundle' => $group_type->id(),
      'label' => 'Permission set',
      // Since permission sets are also configuration, to keep things simple for
      // administrators and prevent confusion in understanding which set is used
      // we set this field as required. That is, groups the types of which have
      // permission sets enabled must reference a permission set. Even though
      // the permission calculators could fall back to the permissions provided
      // by the group roles at the group type level, from a performance
      // perspective we do not want to calculate both and it is a cleaner
      // solution to create a default permission set and use that instead.
      'required' => TRUE,
      'translatable' => FALSE,
    ]);
    $config_storage->save($config);
  }

  /**
   * Installs the storage for the permission set field.
   *
   * @return \Drupal\field\Entity\FieldStorageConfig
   *   The field storage configuration entity.
   */
  protected function installFieldStorage(): FieldStorageConfig {
    $storage = $this->entityTypeManager->getStorage('field_storage_config');

    $config = $storage->load('group.' . GroupField::PERMISSION_SET);
    if ($config) {
      return $config;
    }

    $config = $storage->create([
      'field_name' => GroupField::PERMISSION_SET,
      'entity_type' => 'group',
      'type' => 'entity_reference',
      'cardinality' => 1,
      'settings' => ['target_type' => EntityType::PERMISSION_SET],
      'translatable' => FALSE,
      'locked' => TRUE,
    ]);
    $storage->save($config);

    return $config;
  }

  /**
   * Uninstalls the permission set field from the given group type.
   *
   * @param Drupal\group\Entity\GroupTypeInterface $group_type
   *   The group type.
   */
  protected function uninstallField(GroupTypeInterface $group_type): void {
    $config_storage = $this->entityTypeManager->getStorage('field_config');
    $config = $config_storage->load(
      'group.' . $group_type->id() . '.' . GroupField::PERMISSION_SET
    );
    if (!$config) {
      return;
    }

    $config_storage->delete([$config]);
  }

}

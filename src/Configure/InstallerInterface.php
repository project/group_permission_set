<?php

namespace Drupal\group_permission_set\Configure;

use Drupal\group\Entity\GroupTypeInterface;

/**
 * Installs configuration as expected by Group Permission Set functionality.
 *
 * The installer make it easy to enable/disable permission sets on group types.
 *
 * Note that the expected behavior of the Installer is to install expected
 * configuration if it doesn't exist, but to not throw exceptions if it is
 * already installed. That way, it can be safely run again as additions are
 * being made without errors on previously installed configuration. Uninstalling
 * is different though; exceptions should be thrown when trying to uninstall
 * something that is not installed.
 */
interface InstallerInterface {

  /**
   * Enables permission sets for the given group type.
   *
   * It includes:
   *   - Installing the required bundle field on the group type.
   *
   * @param Drupal\group\Entity\GroupTypeInterface $group_type
   *   The group type for which to enable permission sets.
   * @param bool $save
   *   `TRUE` to save the group type, `FALSE` otherwise. You may want to set
   *   this to `FALSE` if the caller of this method will be saving the group
   *   type anyways.
   */
  public function installForGroupType(
    GroupTypeInterface $group_type,
    bool $save = TRUE
  ): void;

  /**
   * Disables permission sets for the given group type.
   *
   * It includes:
   *   - Uninstalling the required bundle field on the group type.
   *
   * @param Drupal\group\Entity\GroupTypeInterface $group_type
   *   The group type for which to disable permission sets.
   * @param bool $save
   *   `TRUE` to save the group type, `FALSE` otherwise. You may want to set
   *   this to `FALSE` if the caller of this method will be saving the group
   *   type anyways.
   */
  public function uninstallForGroupType(
    GroupTypeInterface $group_type,
    bool $save = TRUE
  ): void;

  /**
   * Returns whether permission sets are enabled for the given group type.
   *
   * @param Drupal\group\Entity\GroupTypeInterface $group_type
   *   The group type to check.
   *
   * @return bool
   *   `TRUE` if permission sets are enabled for the group type, `FALSE`
   *   otherwise.
   *
   * @see $this::installForGroupType()
   */
  public function isInstalledForGroupType(GroupTypeInterface $group_type): bool;

  /**
   * Returns whether permission sets can be disabled for the given group type.
   *
   * It should prevent disabling permission sets on a group type that already
   * has permission sets available to it. The permission sets will need to be
   * removed first.
   *
   * @param Drupal\group\Entity\GroupTypeInterface $group_type
   *   The group type to check.
   *
   * @return bool
   *   `TRUE` if permission sets can be uninstalled for the group type, `FALSE`
   *   otherwise.
   */
  public function canUninstallForGroupType(GroupTypeInterface $group_type): bool;

}

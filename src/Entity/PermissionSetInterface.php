<?php

namespace Drupal\group_permission_set\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the interface for the Permission Set configuration entity.
 */
interface PermissionSetInterface extends
  ConfigEntityInterface,
  EntityDescriptionInterface {

  /**
   * Returns the permissions defined by the permission set.
   *
   * @return array
   *   An associative array containing the permissions keyed by group role and
   *   containing a numerical array with the permissions granted to the role.
   */
  public function getPermissions(): array;

}

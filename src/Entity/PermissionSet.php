<?php

declare(strict_types=1);

namespace Drupal\group_permission_set\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * The default implementation of the Permission Set configuration entity.
 *
 * @ConfigEntityType(
 *   id = "group_permission_set",
 *   label = @Translation("Group permission set"),
 *   label_singular = @Translation("group permission set"),
 *   label_plural = @Translation("group permission sets"),
 *   label_count = @PluralTranslation(
 *     singular = "@count group permission set",
 *     plural = "@count group permission sets"
 *   ),
 *   admin_permission = "administer group_permission_set",
 *   config_prefix = "set",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "uuid",
 *     "label",
 *     "description",
 *     "group_type_id",
 *     "permissions",
 *   },
 * )
 */
class PermissionSet extends ConfigEntityBase implements PermissionSetInterface {

  /**
   * The machine name of the permission set.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the permission set.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of the permission set.
   *
   * @var string
   */
  protected $description;

  /**
   * The ID of the group type that the permission set is available to.
   *
   * @var string
   */
  protected $group_type_id;

  /**
   * The permissions that the set defines.
   *
   * Permissions are defined as an associative array containing all permissions
   * granted to all roles available to the group type. Each array item is keyed
   * by the ID of the group role and it contains a numerical array with the
   * permissions granted to the role.
   *
   * Roles flagged as administrator roles i.e. granted all permissions, should
   * be contain an empty array of permissions.
   *
   * @var array
   */
  protected $permissions;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPermissions(): array {
    return $this->permissions;
  }

}

<?php

declare(strict_types=1);

namespace Drupal\group_permission_set\Access;

// Drupal modules.
use Drupal\flexible_permissions\CalculatedPermissionsItem;
use Drupal\flexible_permissions\PermissionCalculatorBase;
use Drupal\group\PermissionScopeInterface;
// Drupal core.
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Calculates synchronized group permissions for an account.
 *
 * We replace the calculator provided by the Group module for two reasons.
 * - Most importantly, for efficiency. If we were to add a new calculator that
 *   would run on top of the default, group role-based permissions would be
 *   calculated when they are not needed i.e. when permission sets are
 *   enabled. Also, a bunch of computations and I/O would be done twice,
 *   including loading all roles and group types in the system.
 * - Additionally, it simplifies the logic. Altering or replacing the
 *   permissions provided by the default calculator is doable but makes the code
 *   unnecessarily complex. Much better to calculate just the permissions that
 *   we need depending on whether group type has permission sets enabled.
 */
class SynchronizedGroupPermissionCalculator extends PermissionCalculatorBase {

  /**
   * Constructs a SynchronizedGroupPermissionCalculator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function calculatePermissions(AccountInterface $account, $scope) {
    if (!$this->isScopeSynchronized($scope)) {
      return parent::calculatePermissions($account, $scope);
    }

    $calculated_permissions = parent::calculatePermissions($account, $scope);

    // If a new group role is introduced, we need to recalculate the permissions
    // for the provided scope.
    $calculated_permissions->addCacheTags(['config:group_role_list']);

    $roles = $this->entityTypeManager
      ->getStorage('group_role')
      ->loadByProperties([
        'scope' => $scope,
        'global_role' => $account->getRoles(),
      ]);
    $permission_sets = $this->entityTypeManager
      ->getStorage('group_permission_set')
      ->loadMultiple();

    foreach ($roles as $role) {
      $status = $role->getGroupType()->getThirdPartySetting(
        'group_permission_set',
        'status',
      );

      if (empty($status)) {
        $item = new CalculatedPermissionsItem(
          $role->getScope(),
          $role->getGroupTypeId(),
          $role->getPermissions(),
          $role->isAdmin()
        );
        $calculated_permissions->addItem($item);
        $calculated_permissions->addCacheableDependency($role);
        continue;
      }

      foreach ($permission_sets as $permission_set) {
        $set_permissions = $permission_set->getPermissions();
        if (!isset($set_permissions[$role->id()])) {
          continue;
        }

        $item = new CalculatedPermissionsItem(
          $role->getScope(),
          'set:' . $permission_set->id(),
          $set_permissions[$role->id()],
          $role->isAdmin()
        );
        $calculated_permissions->addItem($item);
        $calculated_permissions->addCacheableDependency($role);
        $calculated_permissions->addCacheableDependency($permission_set);
      }
    }

    return $calculated_permissions;
  }

  /**
   * {@inheritdoc}
   */
  public function getPersistentCacheContexts($scope) {
    if ($this->isScopeSynchronized($scope)) {
      return ['user.roles'];
    }

    return [];
  }

  /**
   * Returns whether the given scope is synchronized.
   *
   * @param string $scope
   *   The scope.
   *
   * @return bool
   *   `TRUE` if the scope is synchronized, `FALSE` otherwise.
   */
  public function isScopeSynchronized(string $scope): bool {
    $synchronized_scopes = [
      PermissionScopeInterface::OUTSIDER_ID,
      PermissionScopeInterface::INSIDER_ID,
    ];
    if (in_array($scope, $synchronized_scopes, TRUE)) {
      return TRUE;
    }

    return FALSE;
  }

}

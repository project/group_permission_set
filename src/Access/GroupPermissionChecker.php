<?php

declare(strict_types=1);

namespace Drupal\group_permission_set\Access;

// Drupal modules.
use Drupal\flexible_permissions\CalculatedPermissionsInterface;
use Drupal\group\Access\GroupPermissionCheckerInterface;
use Drupal\group\Access\GroupPermissionCalculatorInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\GroupMembershipLoaderInterface;
use Drupal\group\PermissionScopeInterface;
use Drupal\group_permission_set\Constant\Field\Group as GroupField;
// Drupal core.
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Calculates group permissions for an account.
 *
 * For groups that their types have permission sets enabled, we need to alter
 * the identifiers of calculated permissions. We therefore need to replace the
 * permission checker provided by the group module with our own.
 */
class GroupPermissionChecker implements GroupPermissionCheckerInterface {

  /**
   * Holds the permission sets status per group type.
   *
   * Keyed by group type ID and containing the status. For efficiency, so we
   * don't have to recalculate it multiple times per group type.
   *
   * @var array
   */
  protected $statuses = [];

  /**
   * Constructs a GroupPermissionChecker object.
   *
   * @param \Drupal\group\Access\GroupPermissionCalculatorInterface $permissionCalculator
   *   The group permission calculator.
   * @param \Drupal\group\GroupMembershipLoaderInterface $membershipLoader
   *   The group membership loader.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    protected GroupPermissionCalculatorInterface $permissionCalculator,
    protected GroupMembershipLoaderInterface $membershipLoader,
    protected EntityTypeManagerInterface $entityTypeManager
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function hasPermissionInGroup(
    $permission,
    AccountInterface $account,
    GroupInterface $group
  ) {
    $calculated_permissions = $this->permissionCalculator
      ->calculateFullPermissions($account);

    // First check if the user has the permission based on an individual
    // membership.
    $item = $calculated_permissions->getItem(
      PermissionScopeInterface::INDIVIDUAL_ID,
      $group->id()
    );
    if ($item && $item->hasPermission($permission)) {
      return TRUE;
    }

    // Otherwise, check permissions using permission sets if enabled, or using
    // the group type's group roles if disabled.
    $status = $this->getStatus($group->bundle());
    if ($status === TRUE) {
      return $this->hasPermissionByPermissionSet(
        $permission,
        $account,
        $group,
        $calculated_permissions
      );
    }

    return $this->hasPermissionByGroupRoles(
      $permission,
      $account,
      $group,
      $calculated_permissions
    );
  }

  /**
   * Returns whether permission sets are enabled for the given group type.
   *
   * @param string $group_type_id
   *   The ID of the group type.
   *
   * @return bool
   *   `TRUE` if enabled, `FALSE` otherwise.
   */
  protected function getStatus(string $group_type_id): bool {
    if (isset($this->statuses[$group_type_id])) {
      return $this->statuses[$group_type_id];
    }

    $this->statuses[$group_type_id] = $this->entityTypeManager
      ->getStorage('group_type')
      ->load($group_type_id)
      ->getThirdPartySetting(
        'group_permission_set',
        'status'
      );
    return $this->statuses[$group_type_id];
  }

  /**
   * Checks whether an account has a permission based on group roles.
   *
   * @param string $permission
   *   The permission to check.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account to check the permission for.
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group.
   * @param \Drupal\flexible_permissions\CalculatedPermissionsInterface $calculated_permissions
   *   The calculated permissions for the account.
   *
   * @return bool
   *   `TRUE` if the account has the group permission, `FALSE` otherwise.
   */
  protected function hasPermissionByGroupRoles(
    string $permission,
    AccountInterface $account,
    GroupInterface $group,
    CalculatedPermissionsInterface $calculated_permissions
  ): bool {
    $item = NULL;

    if ($this->membershipLoader->load($group, $account)) {
      $item = $calculated_permissions->getItem(
        PermissionScopeInterface::INSIDER_ID,
        $group->bundle()
      );
    }
    else {
      $item = $calculated_permissions->getItem(
        PermissionScopeInterface::OUTSIDER_ID,
        $group->bundle()
      );
    }

    return $item !== NULL && $item->hasPermission($permission);
  }

  /**
   * Checks whether an account has a permission based on permission sets.
   *
   * @param string $permission
   *   The permission to check.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account to check the permission for.
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group.
   * @param \Drupal\flexible_permissions\CalculatedPermissionsInterface $calculated_permissions
   *   The calculated permissions for the account.
   *
   * @return bool
   *   `TRUE` if the account has the group permission, `FALSE` otherwise.
   */
  protected function hasPermissionByPermissionSet(
    string $permission,
    AccountInterface $account,
    GroupInterface $group,
    CalculatedPermissionsInterface $calculated_permissions
  ): bool {
    if (!$group->hasField(GroupField::PERMISSION_SET)) {
      throw new \RuntimeException(sprintf(
        'Group type "%s" has permission sets enabled but the required bundle field does not exist.',
        $group->bundle()
      ));
    }

    $permission_set = $group->get(GroupField::PERMISSION_SET)->entity;
    if (!$permission_set) {
      throw new \RuntimeException(sprintf(
        'Group type "%s" has permission sets enabled but group with ID "%s" does not reference any permission set.',
        $group->bundle(),
        $group->id()
      ));
    }

    $item = NULL;
    if ($this->membershipLoader->load($group, $account)) {
      $item = $calculated_permissions->getItem(
        PermissionScopeInterface::INSIDER_ID,
        'set:' . $permission_set->id()
      );
    }
    else {
      $item = $calculated_permissions->getItem(
        PermissionScopeInterface::OUTSIDER_ID,
        'set:' . $permission_set->id()
      );
    }

    return $item && $item->hasPermission($permission);
  }

}

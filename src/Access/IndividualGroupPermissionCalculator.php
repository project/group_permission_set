<?php

declare(strict_types=1);

namespace Drupal\group_permission_set\Access;

// Drupal modules.
use Drupal\flexible_permissions\CalculatedPermissionsItem;
use Drupal\flexible_permissions\PermissionCalculatorBase;
use Drupal\flexible_permissions\RefinableCalculatedPermissionsInterface;
use Drupal\group\GroupMembership;
use Drupal\group\GroupMembershipLoaderInterface;
use Drupal\group\PermissionScopeInterface;
use Drupal\group_permission_set\Constant\Field\Group as GroupField;
// Drupal core.
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Calculates individual group permissions for an account.
 *
 * We replace the calculator provided by the Group module for two reasons.
 * - Most importantly, for efficiency. If we were to add a new calculator that
 *   would run on top of the default, group role-based permissions would be
 *   calculated when they are not needed i.e. when permission sets are
 *   enabled. Also, a bunch of computations and I/O would be done twice,
 *   including loading all memberships for the user.
 * - Additionally, it simplifies the logic. Altering or replacing the
 *   permissions provided by the default calculator is doable but makes the code
 *   unnecessarily complex. Much better to calculate just the permissions that
 *   we need depending on whether group type has permission sets enabled.
 */
class IndividualGroupPermissionCalculator extends PermissionCalculatorBase {

  /**
   * Holds the permission sets status per group type.
   *
   * Keyed by group type ID and containing the status. For efficiency, so we
   * don't have to recalculate it multiple times per group type.
   *
   * @var array
   *
   * @I Share statuses static cache with GroupPermissionChecker
   *    type     : task
   *    priority : low
   *    labels   : performance
   */
  protected $statuses = [];

  /**
   * Constructs a IndividualGroupPermissionCalculator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\group\GroupMembershipLoaderInterface $membershipLoader
   *   The group membership loader.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected GroupMembershipLoaderInterface $membershipLoader
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function calculatePermissions(AccountInterface $account, $scope) {
    $calculated_permissions = parent::calculatePermissions($account, $scope);

    if ($scope !== PermissionScopeInterface::INDIVIDUAL_ID) {
      return $calculated_permissions;
    }

    // The member permissions need to be recalculated whenever the user is added
    // to or removed from a group.
    $calculated_permissions->addCacheTags([
      'group_relationship_list:plugin:group_membership:entity:' . $account->id(),
    ]);

    // Following the pattern implemented by the Group module, we calculate the
    // permissions for all memberships for the users. They are calculated once
    // and cached, and they are not needed to be calculated again when the same
    // user visits another group that is member of. This is fine when the number
    // of memberships is fairly limited, but it might not be ideal in sites
    // where users may be memberships of dozens of groups, or more.
    // phpcs:disable
    // @I Review permission calculation strategy for large sites
    //    type     : task
    //    priority : normal
    //    labels   : performance
    // phpcs:enable
    foreach ($this->membershipLoader->loadByUser($account) as $membership) {
      // If the member's roles change, so do the permissions.
      $calculated_permissions->addCacheableDependency($membership);

      $status = $this->getStatus($membership->getGroup()->bundle());
      if ($status === TRUE) {
        $this->calculatePermissionsByPermissionSet(
          $membership,
          $calculated_permissions
        );
      }
      else {
        $this->calculatePermissionsByGroupRoles(
          $membership,
          $calculated_permissions
        );
      }
    }

    return $calculated_permissions;
  }

  /**
   * {@inheritdoc}
   */
  public function getPersistentCacheContexts($scope) {
    if ($scope === PermissionScopeInterface::INDIVIDUAL_ID) {
      return ['user'];
    }

    return [];
  }

  /**
   * Returns whether permission sets are enabled for the given group type.
   *
   * @param string $group_type_id
   *   The ID of the group type.
   *
   * @return bool
   *   `TRUE` if enabled, `FALSE` otherwise.
   */
  protected function getStatus(string $group_type_id): bool {
    if (isset($this->statuses[$group_type_id])) {
      return $this->statuses[$group_type_id];
    }

    $this->statuses[$group_type_id] = $this->entityTypeManager
      ->getStorage('group_type')
      ->load($group_type_id)
      ->getThirdPartySetting(
        'group_permission_set',
        'status'
      );
    return $this->statuses[$group_type_id];
  }

  /**
   * Adds items to calculated permissions based on group roles.
   *
   * @param \Drupal\group\GroupMembership $membership
   *   The group membership.
   * @param \Drupal\flexible_permissions\RefinableCalculatedPermissionsInterface $calculated_permissions
   *   The calculated permissions to add items to.
   */
  protected function calculatePermissionsByGroupRoles(
    GroupMembership $membership,
    RefinableCalculatedPermissionsInterface $calculated_permissions
  ): void {
    foreach ($membership->getRoles(FALSE) as $role) {
      $item = new CalculatedPermissionsItem(
        $role->getScope(),
        $membership->getGroup()->id(),
        $role->getPermissions(),
        $role->isAdmin()
      );
      $calculated_permissions->addItem($item);
      // The permissions need to be reclaculated if the role changes.
      $calculated_permissions->addCacheableDependency($role);
    }
  }

  /**
   * Adds items to calculated permissions based on permission sets.
   *
   * @param \Drupal\group\GroupMembership $membership
   *   The group membership.
   * @param \Drupal\flexible_permissions\RefinableCalculatedPermissionsInterface $calculated_permissions
   *   The calculated permissions to add items to.
   */
  protected function calculatePermissionsByPermissionSet(
    GroupMembership $membership,
    RefinableCalculatedPermissionsInterface $calculated_permissions
  ): void {
    $group = $membership->getGroup();
    if (!$group->hasField(GroupField::PERMISSION_SET)) {
      throw new \RuntimeException(sprintf(
        'Group type "%s" has permission sets enabled but the required bundle field does not exist.',
        $group->bundle()
      ));
    }

    $permission_set = $group->get(GroupField::PERMISSION_SET)->entity;
    if (!$permission_set) {
      throw new \RuntimeException(sprintf(
        'Group type "%s" has permission sets enabled but group with ID "%s" does not reference any permission set.',
        $group->bundle(),
        $group->id()
      ));
    }

    $set_permissions = $permission_set->getPermissions();
    foreach ($membership->getRoles(FALSE) as $role) {
      $item = new CalculatedPermissionsItem(
        $role->getScope(),
        $group->id(),
        $set_permissions[$role->id()],
        $role->isAdmin()
      );
      $calculated_permissions->addItem($item);
      // The permissions need to be reclaculated if the role changes, but also
      // if the permission set referenced by the group is changed, and if the
      // permission set itself is changed.
      $calculated_permissions->addCacheableDependency($role);
      $calculated_permissions->addCacheableDependency($group);
      $calculated_permissions->addCacheableDependency($permission_set);
    }
  }

}

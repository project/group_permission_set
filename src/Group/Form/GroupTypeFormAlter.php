<?php

declare(strict_types=1);

namespace Drupal\group_permission_set\Group\Form;

// Drupal modules.
use Drupal\group_permission_set\Configure\InstallerInterface;
// Drupal core.
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Holds methods implementing alteration on group type forms.
 */
class GroupTypeFormAlter {

  use StringTranslationTrait;

  /**
   * Constructs a new GroupTypeFormAlter object.
   *
   * @param \Drupal\group_permission_set\Configure\InstallerInterface $installer
   *   The installer.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(
    protected InstallerInterface $installer,
    protected MessengerInterface $messenger,
    TranslationInterface $string_translation
  ) {
    // Properties defined in traits.
    $this->stringTranslation = $string_translation;
  }

  /**
   * Implements hook_form_FORM_ID_alter().
   *
   * Handles permission sets settings for group types. We add a checkbox for
   * enabling/disabling permission sets the group type being created/edited.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function alterForm(
    array &$form,
    FormStateInterface $form_state
  ): void {
    $group_type = $form_state->getFormObject()->getEntity();

    $description = $this->t(
      'When enabled, you will be able to create permission sets for this group
       type. Users with the appropriate permissions will then be able to
       choose which set each group of this type will use for its access
       control.'
    );

    $disabled = FALSE;
    $can_uninstall = NULL;
    $is_installed = $this->installer->isInstalledForGroupType($group_type);
    if ($is_installed) {
      $can_uninstall = $this->installer->canUninstallForGroupType($group_type);
    }

    if ($is_installed && !$can_uninstall) {
      $disabled = TRUE;
      $description .= '<p><em>' . $this->t(
        'You cannot disable permission sets on this group type before removing
         all existing permission sets for it.'
      ) . '</em></p>';
    }

    $form['group_permission_set'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#tree' => TRUE,
      '#title' => $this->t('Permission sets'),
    ];
    $form['group_permission_set']['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable permission sets'),
      '#description' => $description,
      '#default_value' => $is_installed,
      '#disabled' => $disabled,
    ];

    $form['actions']['submit']['#validate'][] = 'group_permission_set_group_type_form_validate';

    // We want our submit callback to run before the existing `::save` callback
    // so that changes we make - adding a third party setting - are saved
    // without having to save the entity again. We do not use an entity builder
    // for that because, for consistency and modularity, we want to use the
    // installer service for handling this including setting the third party
    // service.
    array_splice(
      $form['actions']['submit']['#submit'],
      array_search('::save', $form['actions']['submit']['#submit']),
      0,
      ['group_permission_set_group_type_form_submit']
    );
  }

  /**
   * Validates a group type form submission.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateForm(
    array $form,
    FormStateInterface $form_state
  ): void {
    $group_type = $form_state->getFormObject()->getEntity();
    $status = $form_state->getValue(['group_permission_set', 'status']);

    $is_installed = $this->installer->isInstalledForGroupType($group_type);

    $can_uninstall = NULL;
    if ($is_installed) {
      $can_uninstall = $this->installer->canUninstallForGroupType($group_type);
    }

    if (!$status && $is_installed && !$can_uninstall) {
      $form_state->setError(
        $form['group_permission_set']['status'],
        $this->t(
          'You cannot disable permission sets on this group type before removing
           all existing permission sets for it.'
        )
      );
    }
  }

  /**
   * Takes additional actions when a group type form is submitted.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitForm(
    array $form,
    FormStateInterface $form_state
  ): void {
    $group_type = $form_state->getFormObject()->getEntity();
    $status = $form_state->getValue(['group_permission_set', 'status']);

    $is_installed = $this->installer->isInstalledForGroupType($group_type);

    if ($status && !$is_installed) {
      $this->installer->installForGroupType($group_type, FALSE);
      $this->messenger->addStatus($this->t(
        'Permission sets have been successfully enabled on the %group_type group
         type.',
        ['%group_type' => $group_type->label()]
      ));
    }
    elseif (!$status && $is_installed) {
      $this->installer->uninstallForGroupType($group_type, FALSE);
      $this->messenger->addStatus($this->t(
        'Permission sets have been successfully uninstalled from the %group_type
         group type.',
        ['%group_type' => $group_type->label()]
      ));
    }
  }

}

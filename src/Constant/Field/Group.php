<?php

namespace Drupal\group_permission_set\Constant\Field;

/**
 * Holds machine names of Group entity fields.
 *
 * @link https://github.com/krystalcode/drupal8-coding-standards/blob/master/Fields.md#field-name-constants
 */
class Group {

  /**
   * Holds the permission set of the Group entity.
   *
   * The permission set referenced by this field will be the one that will be
   * used for access control for the group, instead of the permissions
   * determined on the group type configuration entity.
   */
  const PERMISSION_SET = 'group_permission_set';

}

<?php

namespace Drupal\group_permission_set\Constant\Entity;

/**
 * Holds IDs of entity types.
 */
class Type {

  /**
   * Holds the ID of the permission set entity type.
   *
   * @see Drupal\group_permission_set\Entity\PermissionSetInterface
   */
  const PERMISSION_SET = 'group_permission_set';

}

<?php

declare(strict_types=1);

use Drupal\group\Entity\GroupTypeInterface;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * @file
 * Hooks and functionality for the Group Permission Set module.
 */

/**
 * Hooks.
 */

/**
 * {@inheritdoc}
 *
 * @see hook_module_implements_alter()
 * @see group_query_entity_query_alter()
 * @see group_query_views_entity_query_alter()
 */
function group_permission_set_module_implements_alter(
  &$implementations,
  $hook,
) {
  $hooks = ['query_entity_query_alter', 'query_views_entity_query_alter'];
  if (!\in_array($hook, $hooks, TRUE)) {
    return;
  }

  // We remove the query alterations provided by the `group` module since we
  // provide our own.
  unset($implementations['group']);

  // We make sure that our query alterations are the last to run.
  $group = $implementations['group_permission_set'];
  unset($implementations['group_permission_set']);
  $implementations['group_permission_set'] = $group;
}

/**
 * {@inheritdoc}
 *
 * @see hook_query_TAG_alter()
 * @see group_query_entity_query_alter()
 */
function group_permission_set_query_entity_query_alter(
  AlterableInterface $query,
) {
  if (!$query instanceof SelectInterface) {
    return;
  }

  $entity_type_id = $query->getMetaData('entity_type');
  if (!$query->hasTag("{$entity_type_id}_access")) {
    return;
  }

  \Drupal::service('group_permission_set.access.entity_query')->alter(
    $query,
    $entity_type_id,
  );
}

/**
 * {@inheritdoc}
 *
 * @see hook_query_TAG_alter()
 * @see group_query_views_entity_query_alter()
 */
function group_permission_set_query_views_entity_query_alter(
  AlterableInterface $query,
) {
  if (!$query instanceof SelectInterface) {
    return;
  }

  $query->addTag($query->getMetaData('entity_type') . '_access');
  group_permission_set_query_entity_query_alter($query);
}

/**
 * {@inheritdoc}
 *
 * @see hook_form_FORM_ID_alter()
 * @see \Drupal\group_permission_set\Group\Form\GroupTypeFormAlter::alterForm()
 */
function group_permission_set_form_group_type_edit_form_alter(
  array &$form,
  FormStateInterface $form_state
) {
  \Drupal::service('group_permission_set.form.group_type_form_alter')
    ->alterForm($form, $form_state);
}

/**
 * {@inheritdoc}
 *
 * @see hook_form_FORM_ID_alter()
 * @see \Drupal\group_permission_set\Group\Form\GroupTypeFormAlter::alterForm()
 */
function group_permission_set_form_group_type_add_form_alter(
  array &$form,
  FormStateInterface $form_state
) {
  \Drupal::service('group_permission_set.form.group_type_form_alter')
    ->alterForm($form, $form_state);
}

/**
 * Callbacks.
 */

/**
 * Validates a group type form submission.
 *
 * @param array $form
 *   The form render array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.

 * @see \Drupal\group_permission_set\Group\Form\GroupTypeFormAlter::validateForm()
 */
function group_permission_set_group_type_form_validate(
  array $form,
  FormStateInterface $form_state
) {
  \Drupal::service('group_permission_set.form.group_type_form_alter')
    ->validateForm($form, $form_state);
}

/**
 * Takes additional actions when a group type form is submitted.
 *
 * @param array $form
 *   The form render array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.

 * @see \Drupal\group_permission_set\Group\Form\GroupTypeFormAlter::validateForm()
 */
function group_permission_set_group_type_form_submit(
  array $form,
  FormStateInterface $form_state
) {
  \Drupal::service('group_permission_set.form.group_type_form_alter')
    ->submitForm($form, $form_state);
}
